/**=============================================================================

@file
   hcp_perf_imp.cpp

@brief
   implementation file for testing hcp_perf.

All Rights Reserved Qualcomm Proprietary
Copyright (c) 2017, 2020 Qualcomm Technologies Incorporated.
=============================================================================**/

//==============================================================================
// Include Files
//==============================================================================

// enable message outputs for profiling by defining _DEBUG and including HAP_farf.h
#ifndef _DEBUG
#define _DEBUG
#endif

#include "HAP_farf.h"
#undef FARF_HIGH
#define FARF_HIGH 1

#include "HAP_perf.h"
#include "HAP_power.h"

#include "hcp_perf.h"
#include "worker_pool.h"

#include "AEEStdErr.h"

// includes
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "hexagon_protos.h"
#include "hexagon_types.h"
#include "HalideRuntime.h"
#include "HalideRuntimeHexagonHost.h"
#include "ResizeNearestNeighbor.h"

/*===========================================================================
    DEFINITIONS
===========================================================================*/

typedef struct
{
    worker_synctoken_t  *token;            // worker pool token
    const run_control_t* rc;
    int input_width;
    int input_height;
    int output_width;
    int output_height;
    int channels;
    instrumentation_t* instrumentation;
    int32_t retval;
} run_callback_t;


// Placeholder structure to create a unique handle for a FastRPC session
typedef struct {
    int element;
} hcp_perf_context_t;



/*===========================================================================
    DECLARATIONS
===========================================================================*/

static int setClocks(remote_handle64 h)
{
    HAP_power_request_t request;
    memset(&request, 0, sizeof(HAP_power_request_t)); //Important to clear the structure if only selected fields are updated.
    request.type = HAP_power_set_apptype;
    request.apptype = HAP_POWER_COMPUTE_CLIENT_CLASS;
    hcp_perf_context_t *hcp_perf_ctx = (hcp_perf_context_t*) h;
    int retval = HAP_power_set((void*) hcp_perf_ctx, &request);
    if (retval) return AEE_EFAILED;

    // Configure clocks & DCVS mode
    memset(&request, 0, sizeof(HAP_power_request_t)); //Important to clear the structure if only selected fields are updated.
    request.type = HAP_power_set_DCVS_v2;
    request.dcvs_v2.dcvs_enable = 0;   // enable dcvs if desired, else it locks to target corner
    request.dcvs_v2.set_dcvs_params = TRUE;
    request.dcvs_v2.dcvs_params.target_corner = HAP_DCVS_VCORNER_NOM; // nominal voltage corner.
    request.dcvs_v2.set_latency = TRUE;
    request.dcvs_v2.latency = 100;
    retval = HAP_power_set((void*) hcp_perf_ctx, &request);
    if (retval) return AEE_EFAILED;

    return 0;
}

/// asdfasdfasda
#include "HAP_perf.h"
#include "HAP_power.h"

enum hvx_perf_mode {
    low = 0,
    nominal = 1,
    turbo = 2
};

typedef enum hvx_perf_mode hvx_perf_mode_t;

int power_on_hvx() {
    HAP_power_request_t request;
    request.type = HAP_power_set_HVX;
    request.hvx.power_up = TRUE;
    int result = HAP_power_set(NULL, &request);
    return result;
}
int power_off_hvx() {
    HAP_power_request_t request;
    request.type = HAP_power_set_HVX;
    request.hvx.power_up = FALSE;
    int result = HAP_power_set(NULL, &request);
    return result;
}
int set_hvx_perf(int set_mips,
                 unsigned int mipsPerThread,
                 unsigned int mipsTotal,
                 int set_bus_bw,
                 unsigned int bwMegabytesPerSec,
                 unsigned int busbwUsagePercentage,
                 int set_latency,
                 int latency) {
    HAP_power_request_t request;

    request.type = HAP_power_set_apptype;
    request.apptype = HAP_POWER_COMPUTE_CLIENT_CLASS;
    int retval = HAP_power_set(NULL, &request);
    if (0 != retval) {
        FARF(LOW, "HAP_power_set(HAP_power_set_apptype) failed (%d)\n", retval);
        return -1;
    }

    request.type = HAP_power_set_mips_bw;
    request.mips_bw.set_mips = set_mips;
    request.mips_bw.mipsPerThread = mipsPerThread;
    request.mips_bw.mipsTotal = mipsTotal;
    request.mips_bw.set_bus_bw = set_bus_bw;
    request.mips_bw.bwBytePerSec = ((uint64_t)bwMegabytesPerSec) << 20;
    request.mips_bw.busbwUsagePercentage = busbwUsagePercentage;
    request.mips_bw.set_latency = set_latency;
    request.mips_bw.latency = latency;
    retval = HAP_power_set(NULL, &request);
    if (0 != retval) {
        FARF(LOW, "HAP_power_set(HAP_power_set_mips_bw) failed (%d)\n", retval);
        return -1;
    }
    return 0;
}
int set_hvx_perf_mode(hvx_perf_mode_t mode) {
    int set_mips = 0;
    unsigned int mipsPerThread = 0;
    unsigned int mipsTotal = 0;
    int set_bus_bw = 0;
    uint64_t bwBytePerSec = 0;
    unsigned int bwMegabytesPerSec = 0;
    unsigned int busbwUsagePercentage = 0;
    int set_latency = 0;
    int latency = 0;

    HAP_power_response_t power_info;
    unsigned int max_mips = 0;
    uint64 max_bus_bw = 0;

    power_info.type = HAP_power_get_max_mips;
    int retval = HAP_power_get(NULL, &power_info);
    if (0 != retval) {
        FARF(LOW, "HAP_power_get(HAP_power_get_max_mips) failed (%d)\n", retval);
        return -1;
    }
    max_mips = power_info.max_mips;

    // Make sure max_mips is at least sanity_mips
    const unsigned int sanity_mips = 500;
    if (max_mips < sanity_mips) {
        max_mips = sanity_mips;
    }

    power_info.type = HAP_power_get_max_bus_bw;
    retval = HAP_power_get(NULL, &power_info);
    if (0 != retval) {
        FARF(LOW, "HAP_power_get(HAP_power_get_max_bus_bw) failed (%d)\n", retval);
        return -1;
    }
    max_bus_bw = power_info.max_bus_bw;

    // The above API under-reports the max bus bw. If we use it as
    // reported, performance is bad. Experimentally, this only
    // needs to be ~10x.
    // Make sure max_bus_bw is at least sanity_bw
    const uint64 sanity_bw = 1000000000ULL;
    if (max_bus_bw < sanity_bw) {
        if (max_bus_bw == 0) {
            max_bus_bw = sanity_bw;
        }
        while (max_bus_bw < sanity_bw) {
            max_bus_bw <<= 3;  // Increase value while preserving bits
        }
    }

    set_mips = TRUE;
    set_bus_bw = TRUE;
    set_latency = TRUE;

    switch (mode) {
    case low:
        mipsPerThread = max_mips / 4;
        bwBytePerSec = max_bus_bw / 2;
        busbwUsagePercentage = 25;
        latency = 1000;
        break;
    case nominal:
        mipsPerThread = (3 * max_mips) / 8;
        bwBytePerSec = max_bus_bw;
        busbwUsagePercentage = 50;
        latency = 100;
        break;
    case turbo:
    default:
        mipsPerThread = max_mips;
        bwBytePerSec = max_bus_bw * 4;
        busbwUsagePercentage = 100;
        latency = 10;
        break;
    }
    mipsTotal = mipsPerThread * 2;

    bwMegabytesPerSec = bwBytePerSec >> 20;
    return set_hvx_perf(set_mips,
                        mipsPerThread,
                        mipsTotal,
                        set_bus_bw,
                        bwMegabytesPerSec,
                        busbwUsagePercentage,
                        set_latency,
                        latency);
}
int set_hvx_perf_mode_low() {
    return set_hvx_perf_mode(low);
}
int set_hvx_perf_mode_nominal() {
    return set_hvx_perf_mode(nominal);
}
int set_hvx_perf_mode_turbo() {
    return set_hvx_perf_mode(turbo);
}
#if (__HEXAGON_ARCH__ >= 65)
    #include "HAP_vtcm_mgr.h"
#else
    #error "HEXAGON_ARCH < 65 !!!!!"
#endif
#define DSP_SUCCESS 0
#define DSP_FAILURE -1

#define MAX_UNIFORM_SIZE         12
#define MAX_UNIFORM_ELEM_SIZE    512
#define MAX_GAME_UI_BATCHED_JOBS 45
#define MAX_VERTEX_SHADER_COUNT  500000
#define MIN_VERTEX_SHADER_COUNT  2000
#define NUM_B_IN_KB              1024
#define GATHER_SIZE_PER_THREAD   2
/* ------------------ Vertex Shader Macros --------------------------------- */
#define NUM_VERTICES_PER_RECTANGLE 4
#define NEON_LANES                 4
/* ------------------------------------------------------------------------- */
// HVX Vector length - 128bytes
// NEON alignment : 128 bits
#define VLEN (128)

/* HVX definitions */
typedef long HEXAGON_Vect1024_UN __attribute__((__vector_size__(128))) __attribute__((aligned(4)));
#define vmemu(A) *((HEXAGON_Vect1024_UN*) (A))

const unsigned int VTCM_GATHER_MIN_SZ = 32 * 1024; // VTCM max size = 256K --> 256k / (2zones(src+dst) * 4threads) = 32k

typedef struct {
    unsigned int threadCount;      // thread counter shared by all workers
    unsigned char* dst;            // destination for resize function
    unsigned int dstStride;        // stride for Destination
    unsigned int jobHead;          // a counter shared by different threads
    HVX_Vector* vtcmBase;          // a Point of the assigned allocated VTCM memory
} resize_callback_t;

#define L2FETCH(ADDR, REG) asm("  l2fetch(%0, %1)\n" ::"r"(ADDR), "r"(REG));

int min(int a, int b) {
    return a < b? a : b;
}

static void hvx_line_resizeNN(const uint32_t* S, unsigned int srcWidth, uint32_t* Dstart, unsigned int dstWidth, HVX_Vector* vtcmBase, uint32_t* x_offset, int channels,
                              int optimizedWidth, int hvxPixelCount) {
    //FARF(HIGH, "hvx_line_resizeNN(S = %p, srcWidth = %u, Dstart = %p, dstWidth = %u, vtcmBase = %p, x_offset = %p, channels = %i, optimizedWidth = %i , hvxPixelCount = %i)", S, srcWidth, Dstart, dstWidth, vtcmBase, x_offset, channels, optimizedWidth, hvxPixelCount);
#if 1
    HVX_Vector* vSrc     = (HVX_Vector*) S;
    HVX_Vector* vDst     = (HVX_Vector*) Dstart;
    HVX_Vector* vSrcVTCM = (HVX_Vector*) vtcmBase;
    HVX_Vector* vDstVTCM = (HVX_Vector*) vtcmBase + (VTCM_GATHER_MIN_SZ / VLEN);
    HVX_Vector vOffset;
    unsigned int x; // NOLINT

    // Copy texture line to VTCM, 128bytes by 128bytes then 4 bytes by 4 bytes
    int rest = srcWidth * channels % VLEN;
    for (int j = 0; j < srcWidth * channels / VLEN; j++) {
        *vSrcVTCM++ = vmemu(vSrc++);
    }

    uint32_t* vSrc_i     = (uint32_t*) vSrc;
    uint32_t* vSrcVTCM_i = (uint32_t*) vSrcVTCM;
    for (int j = 0; j < rest / channels; j++) {
        *vSrcVTCM_i++ = *vSrc_i++;
    }

    // set base addr for source
    vSrcVTCM = vtcmBase;
    //FARF(HIGH, "gather loop %p", vDst);
    for (x = 0; x < optimizedWidth; x += hvxPixelCount) {
        // Put offsets in 128-bytes vector
        vOffset = *(HVX_Vector*) &x_offset[x];

        // Collect dst from src + offsets via gather function
        Q6_vgather_ARMVw(vDstVTCM++, (int) vSrcVTCM, VTCM_GATHER_MIN_SZ - 1, vOffset);
    }

    // copy back resized from VCTM to memory (n*128bytes)
    vDstVTCM = vtcmBase + (VTCM_GATHER_MIN_SZ / VLEN);
    for (int j = 0; j < optimizedWidth; j += hvxPixelCount) {
#if 1 // ERROR HERE
        vmemu(vDst++) = *vDstVTCM++;
        //FARF(HIGH, "write to %p", vDst);
        //vmemu(vDst++) = vOffset;
#endif
    }
    //FARF(HIGH, "written %p", vDst);

    // non-optimized part of the line
    // TODO: do 32bytes then 4bytes ?
    for (; x < dstWidth; x++) {
        uint32_t data = S[x_offset[x] >> 2];

        // /!\ offset is in bytes not pixels
        Dstart[x] = data;
    }
#endif
}

int hvx_resizeNN(HVX_Vector* vtcmBase,
        halide_buffer_t* input,
        size_t sX_x1024, size_t sY_x1024,
        halide_buffer_t* output) {
    //FARF(HIGH, "calling hvx");

    int c = input->type.bits/8;

    uint8_t* src = input->host;
    size_t srcWidth = input->dim[0].extent;
    size_t srcHeight = input->dim[1].extent;
    size_t srcStride = input->dim[1].stride;

    uint8_t* dst = output->host;
    size_t dstWidth = output->dim[0].extent;
    size_t dstHeight = output->dim[1].extent;
    size_t dstStride = output->dim[1].stride;

    int optimizedWidth       = dstWidth - (dstWidth & ((VLEN >> 2) - 1));

    // Compute offset for a row, offset is in bytes
    // original algorithm:
    //   float x_ratio = srcWidth / dstWidth; x_offset[x] = floor(x * x_ratio) * 4
    // vectorized algorithm:
    //   int x_ratio = srcWidth * 1024 /dstWdith; x_offset[x] = x * x_ratio /1024 * 4
    // The transformation is always true within srcWidth, dstWidth in [1,4096]
    int offset_length      = (dstWidth * sizeof(uint32_t) - 1) / VLEN + 1;
    uint32_t* x_offset     = (uint32_t*) memalign(VLEN, offset_length * VLEN);
    HVX_Vector* x_offset_v = (HVX_Vector*) (x_offset);
    HVX_Vector offset      = Q6_V_vsplat_R(0);
    for (int i = 0; i < 32; ++i) {
        offset[i] = i;
    }

    HVX_Vector v_last  = Q6_Vuw_vmpye_VuhRuh(offset, sX_x1024);
    HVX_Vector v0      = Q6_Vuw_vlsr_VuwR(v_last, 10);
    //x_offset_v[0]      = Q6_Vuw_vmpye_VuhRuh(v0, 4); // TODO c?
    x_offset_v[0]      = Q6_Vuw_vmpye_VuhRuh(v0, c);
    HVX_Vector delta_v = Q6_V_vsplat_R(32 * sX_x1024);

    // At the start of iteration i, v_last is the x*xratio_int for iteration i-1
    for (uint32_t i = 1; i < offset_length; ++i) {
        v_last        = Q6_Vw_vadd_VwVw(v_last, delta_v);
        HVX_Vector v  = Q6_Vuw_vlsr_VuwR(v_last, 10);
        // x_offset_v[i] = Q6_Vuw_vmpye_VuhRuh(v, 4); // TODO c?
        x_offset_v[i] = Q6_Vuw_vmpye_VuhRuh(v, c);
    }

    // Initialize L2 fetch info
    uint64_t L2FETCH_REGISTER = (1ULL << 48) | ((uint64_t)(srcStride * c) << 32) | ((uint64_t)(srcWidth * c) << 16) | 1ULL;

    const unsigned char* imgSrc = &src[0];
    unsigned char* imgDst       = &dst[0];

    L2FETCH(imgSrc, L2FETCH_REGISTER);

    const unsigned char* srcNextLine   = imgSrc;
    const unsigned char* srcCurrenLine = imgSrc;

    int sum = 0;
    for (int i = 0; i < dstHeight; i++) {
        sum += sY_x1024;
        if (i + 1 < dstHeight) {
            int next_y  = min(sum >> 10, srcHeight - 1);
            srcNextLine = imgSrc + next_y * srcStride * (input->type.bits/8);
            // next prefetches will just add 1 row
            // fetch next line is because l2fetch is a non-stop instruction; hexagon processor will continue on other HVX operations while prefetching
#if 1 // L2FETCH crashes if image is too large
            L2FETCH(srcNextLine, L2FETCH_REGISTER);
#endif
        }

        hvx_line_resizeNN((uint32_t*) srcCurrenLine, srcWidth, (uint32_t*) imgDst, dstWidth, vtcmBase, x_offset, c, optimizedWidth, VLEN >> 0); // TODO VLEN >> x crashes when x > 0
        srcCurrenLine = srcNextLine;
        imgDst += dstStride * c;
    }

    free(x_offset);

    return 0;
}

int resizeHVX(
        halide_buffer_t* input,
        size_t sX_x1024, size_t sY_x1024,
        halide_buffer_t* output) {
    HVX_Vector* vtcmBase;
    {
        int numWorkers = 1;
        // Allocate VTCM
        // We want 2* Gather min size per thread:
        // - one for src data
        // - one for dst data
        unsigned int avail_block_size;
        unsigned int max_page_size;
        unsigned int num_pages;
        if (0 == HAP_query_avail_VTCM(&avail_block_size, &max_page_size, &num_pages)) {
            if (avail_block_size < VTCM_GATHER_MIN_SZ * GATHER_SIZE_PER_THREAD * numWorkers) {
                FARF(ERROR, "Available VTCM size less than %d KB (numWorkers*GATHER_SIZE_PER_THREAD*%dKB), aborting...",
                     numWorkers * GATHER_SIZE_PER_THREAD * VTCM_GATHER_MIN_SZ / NUM_B_IN_KB, VTCM_GATHER_MIN_SZ / NUM_B_IN_KB);
                return AEE_ENOMEMORY;
            }
        } else {
            FARF(ERROR, "Fail to call HAP_query_avail_VTCM,  aborting...");
            return AEE_ENOMEMORY;
        }

        vtcmBase = (HVX_Vector*) HAP_request_VTCM(numWorkers * GATHER_SIZE_PER_THREAD * VTCM_GATHER_MIN_SZ, 1);
        if (!vtcmBase) {
            FARF(ERROR, "Could not allocate VTCM, aborting...");
            return AEE_ENOMEMORY;
        }
    }

    hvx_resizeNN(vtcmBase, input, sX_x1024, sY_x1024, output);

    // Release VTCM memory
    HAP_release_VTCM((void*) vtcmBase);

    return 0;
}


int run_nearest(
        const run_control_t* rc,
        int input_width, int input_height,
        int output_width, int output_height,
        int channels,
        instrumentation_t* instr) 
{
    uint32_t x_ratio_x1024 = ((float) input_width / (float) output_width) * 1024.f;
    uint32_t y_ratio_x1024 = ((float) input_height / (float) output_height) * 1024.f;

    halide_buffer_t input = {0};
    halide_buffer_t output = {0};

    halide_dimension_t in_shape[2] = {{0, input_width, 1}, {0, input_height, input_width}};
    halide_dimension_t out_shape[2] = {{0, output_width, 1}, {0, output_height, output_width}};

    input.dimensions = 2;
    output.dimensions = 2;

    input.device = 0;
    output.device = 0;

    input.device_interface = NULL;
    output.device_interface = NULL;

    input.type.code = halide_type_uint;
    output.type.code = halide_type_uint;

    input.type.bits = 8 * channels;
    output.type.bits = 8 * channels;

    input.type.lanes = 1;
    output.type.lanes = 1;

    input.dim = in_shape;
    output.dim = out_shape;

    input.host = (uint8_t*) memalign(128, input_width * input_height * sizeof(uint8_t));
    output.host = (uint8_t*) memalign(128, output_width * output_height * sizeof(uint8_t));

    // Data Initialization
    for (int i = 0; i < input_width * input_height; i ++) {
        input.host[i] = 235; 
    }
    for (int i = 0; i < output_width * output_height; i ++) {
        output.host[i] = 0; 
    }

    uint64_t start_time;
    if(rc->measure_execution_time) {
        start_time = HAP_perf_get_time_us();
    }

    if(rc->use_halide) {
        for (int i = 0; i < rc->iterations; i++) {
            ResizeNearestNeighbor(&input, x_ratio_x1024, y_ratio_x1024, &output);
        }
    }
    else {
        for (int i = 0; i < rc->iterations; i++) {
            resizeHVX(&input, x_ratio_x1024, y_ratio_x1024, &output);
        }
    }

    if(rc->measure_execution_time) {
        uint64_t end_time = HAP_perf_get_time_us();
        instr->mean_execution_time = (unsigned int)((end_time - start_time) / (float) rc->iterations);
    }

    set_hvx_perf_mode_nominal();
    power_off_hvx();

    /**
     *  Output verification
     */
    for (int i = 0; i < output_width * output_height; i ++) {
        if (output.host[i] != 235) {
            FARF(HIGH, "------Error: Failure on verification------ at i = %d; value is %d", i, output.host[i]);
            free(input.host);
            free(output.host);
            return 1;
        }
    }

    FARF(HIGH, "------Verification success------");

    free(input.host);
    free(output.host);

    return 0;
}

static void run_callback(void* data) {
    run_callback_t    *dptr = (run_callback_t*)data;

    dptr->retval = run_nearest(
        dptr->rc,
        dptr->input_width, dptr->input_height,
        dptr->output_width, dptr->output_height,
        dptr->channels,
        dptr->instrumentation
    );

    FARF(HIGH, "SUCCESS");

    worker_pool_synctoken_jobdone(dptr->token);
}

AEEResult hcp_perf_open(const char *uri, remote_handle64 *h)
{
    hcp_perf_context_t *hcp_perf_ctx = malloc(sizeof(hcp_perf_context_t));

    if(hcp_perf_ctx==NULL)
    {
        FARF(ERROR, "Failed to allocate memory for the hcp_perf context");
        return AEE_ENOMEMORY;
    }

    *h = (remote_handle64) hcp_perf_ctx;

    return 0;
}

AEEResult hcp_perf_close(remote_handle64 h)
{
    hcp_perf_context_t *hcp_perf_ctx = (hcp_perf_context_t*) h;

    if(hcp_perf_ctx==NULL)
    {
        FARF(ERROR, "NULL handle received in hcp_perf_close()");
        return AEE_EBADHANDLE;
    }

    free(hcp_perf_ctx);

    return 0;
}

AEEResult hcp_perf_run(remote_handle64 h,
        const run_control_t* rc,
        int input_width, int input_height,
        int output_width, int output_height,
        int channels,
        instrumentation_t* instr
)
{
#if (__HEXAGON_ARCH__ < 65)
    FARF(ERROR, "hcp_perf example supports hexagon architectures >= 65 \n");
    return AEE_EVERSIONNOTSUPPORT;
#endif
    // setClocks will boost clocks
    if (setClocks(h)) return AEE_EFAILED;

    // execute test case from worker thread pool (because these threads have
    // larger stacks than the native thread from the RPC call).
    worker_pool_job_t   job;
    worker_synctoken_t  token;
    worker_pool_context_t context;
    run_callback_t dptr;
    dptr.retval = 0;

    int numWorkers = 1;
    dptr.token = &token;
    dptr.input_width     = input_width;
    dptr.input_height    = input_height;
    dptr.output_width    = output_width;
    dptr.output_height   = output_height;
    dptr.channels        = channels;
    dptr.rc              = rc;
    dptr.instrumentation = instr;
    dptr.retval = -1;
    job.dptr = (void *)&dptr;

    //Init worker pool with custom stack size of each worker thread and get context
    int thread_stack_size = 65536;
    (void)worker_pool_init_with_stack_size(&context, thread_stack_size);
    worker_pool_synctoken_init(&token, numWorkers);
    job.fptr = run_callback;
    for (int i = 0; i < numWorkers; i++) (void) worker_pool_submit(context, job);
    worker_pool_synctoken_wait(&token);
    if (dptr.retval)
    {
        FARF(HIGH, "hcp_perf example test FAILURE 0x%x", dptr.retval);
        goto bail;
    }
    //Deinit worker pool and release resources
    worker_pool_deinit(&context);

    FARF(HIGH, "hcp_perf example test SUCCESS");
bail:
    return dptr.retval;

}
