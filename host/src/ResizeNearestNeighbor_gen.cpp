#include "Halide.h"

using namespace Halide;
class ResizeNearestNeighbor : public Generator<ResizeNearestNeighbor> {
public:
    Input<Buffer<uint8_t>> in{"in", 2};
    Input<int32_t> sX{"sX_x1024"};
    Input<int32_t> sY{"sY_x1024"};
    Output<Buffer<uint8_t>> out{"out", 2};

    void generate() {
        in_vtcm(x, y) = in(x, y);

        Expr x_coord = clamp((x * sX) >> 10, 0, 4000);
        Expr y_coord = clamp((y * sY) >> 10, 0, 4000);

        out_vtcm(x,y) = in_vtcm(x_coord, y_coord);        
        
        out(x,y) = out_vtcm(x,y);

        out.dim(0).set_stride(1);
        in.dim(0).set_stride(1);

        in.dim(0).set_min(0);
        in.dim(1).set_min(0);
        out.dim(0).set_min(0);
        out.dim(1).set_min(0);

        in.set_host_alignment(128);
        out.set_host_alignment(128);
    }

    void schedule() {
            Var xi, yi;

            in_vtcm
                .compute_at(out, y)
                .vectorize(x, 128, TailStrategy::RoundUp)
                .align_storage(x, 128)
                .prefetch(in, yi)
                //.store_root() // can't do this with VTCM
                ;
            out_vtcm
                .compute_at(out, y)
                .vectorize(x, 128, TailStrategy::RoundUp)
                .align_storage(x, 128)
                .prefetch(in_vtcm, yi)
                .store_root()
                ;
            out
                .split(y, y, yi, 64)
                //.parallel(y)
                .vectorize(x, 128)
                ;

            
            in_vtcm.store_in(MemoryType::VTCM);
            out_vtcm.store_in(MemoryType::VTCM);
    }

private:
    Func in_vtcm, out_vtcm;
    Var x{"x"}, y{"y"};

};

HALIDE_REGISTER_GENERATOR(ResizeNearestNeighbor, ResizeNearestNeighbor);
