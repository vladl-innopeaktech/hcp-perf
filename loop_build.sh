#/bin/bash

rm -r build
cmake -B build -S . $@

while [ 1 ]; do 
    cmake --build ./build/;
    if [ $? -ne 0 ]; then
        echo -e "\x1b[31;1;4mFAILURE\033[m";
    else
        echo -e "\x1b[32;1;4mSUCCESS\033[m";
    fi;
    echo "$";
    inotifywait -r -e create,modify ./* 2>/dev/null;
done
